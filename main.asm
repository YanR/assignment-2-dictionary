%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .bss
    buffer: resb 255                 
section .rodata
    buffer_size_err: db "Key is too big",0      
    no_key_err: db "Key wasn't found",0


global _start
extern find_word
section .text                                                       
    _start:
        mov rdi, buffer      
        mov rsi, 255
        call read_word                  
        test rax, rax   
        jz .buffer_error                                  
        mov rdi, buffer                     
        mov rsi, np
        call find_word  
        test rax, rax  
        jz .key_error  
        lea rdi, [rax+8]                                       
        push rdi
        call string_length                                    
        pop rdi
        add rdi, rax             
        inc rdi         
        call print_string  
        jmp .exit
    .buffer_error:
        mov rdi, buffer_size_err                 
        call print_error
        jmp .exit
    .key_error:
        mov rdi, no_key_err
        call print_error
    .exit:
    	call exit
