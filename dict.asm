%include "lib.inc"

global find_word

section .text
find_word:
    .loop:
        push rdi                    
        push rsi                   
        add rsi, 8             
        call string_equals          
        pop rsi                    
        pop rdi
        cmp rax, 1                 
        je .success                
        mov rsi, [rsi]              
        cmp rsi,0                
        je .not_found                  
        jmp .loop
    .success:
        mov rax, rsi               
    .not_found: 
        ret
