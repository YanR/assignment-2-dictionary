ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm 
	$(ASM) $(ASMFLAGS) -o $@ $< 

program: main.o lib.o dict.o
	$(LD) -o program main.o lib.o dict.o

.PHONY: clean, test
clean:
	rm *.o 

test:
	@if [ "$$(echo "first" | ./program 2>&1)" = "Hello world" ] ; \
		then \
			echo "Тест 1 пройден "; \
		else \
			echo "Тест 1 не пройден" ; \
	fi \
	
	@if [ "$$(echo "second" | ./program 2>&1)" = "Something text" ] ; \
		then \
			echo "Тест 2 пройден "; \
		else \
			echo "Тест 2 не пройден" ; \
	fi \
	
	@if [ "$$(echo "third" | ./program 2>&1)" = "Third word" ] ; \
		then \
			echo "Тест 3 пройден "; \
		else \
			echo "Тест 3 не пройден" ; \
	fi \
	
	@if [ "$$(echo "qqqqq" | ./program 2>&1)" = "Key wasn't found" ] ; \
		then \
			echo "Тест 4 пройден "; \
		else \
			echo "Тест 4 не пройден" ; \
	fi \
	
	@if [ "$$(echo "qqqqsadhjgdasjhgduiajpojoidaiukjsaajkgdkasgdkhsdasidhoiashdioashdoiahigsahiodhoiiuhguifasdidgasiuduiagsuduklanssdlkasldnalksnkjbefiuwkgnsakugdsejbdfhvbjnglkkjawgm,dbskjvgklkasndklasndlknaldknaldaifasfkjhsakjssfaljkfhkjabvflkasukjkbgfdkjhaskjdasdassdkjbfsq" | ./program 2>&1)" = "Key is too big" ] ; \
		then \
			echo "Тест 5 пройден "; \
		else \
			echo "Тест 5 не пройден" ; \
	fi \
